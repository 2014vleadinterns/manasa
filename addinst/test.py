import sys
import addinst
import os


def test_address_instructions_1():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("q w u  \n")
	f.write("* 6 s u6")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid input in line 1")

def test_address_instructions_2():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("w q w u  \n")
	f.write("- 6 s u6")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid rator in line 1")

def test_address_instructions_3():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("++ w u t \n")
	f.write("0 6 s u6")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid rator in line 1")

def test_address_instructions_4():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("+ 1_1 u t \n")
	f.write("+ 6 s u6")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid rand1 in line 1")

def test_address_instructions_5():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("-  w ab@    i  \n")
	f.write("0 6 s u6")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid rand2 in line 1")

def test_address_instructions_6():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("- w a_b 12 \n")
	f.write("0 6 s u6")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid rand3 in line 1")


def test_address_instructions_7():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("- w a_b p \n")
	f.write("0 6 s u6")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid rator in line 2")


def test_address_instructions_8():
	os.remove("2.txt")
	f=open("2.txt","a+")
	f.write("- w a_b g \n")
	f.write("* 6 s u6\n")
	f.write("/ 3  2+ abc\n")
	f.write("+ 8 h 7a")
	f.seek(0)
	a=addinst.address_instructions("2.txt")
	assert(a=="invalid rand2 in line 3")

test_address_instructions_1()
test_address_instructions_2()
test_address_instructions_3()
test_address_instructions_4()
test_address_instructions_5()
test_address_instructions_6()
test_address_instructions_7()
test_address_instructions_8()
