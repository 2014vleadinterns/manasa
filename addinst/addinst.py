import sys
import re

def address_instructions(filename):
	f=open(filename,"r")
	f.seek(0)
	data = f.readlines()
	for i in range(0,len(data)):
		l=data[i].split()
		if len(l) is not 4:
			try :
				raise ValueError
			except ValueError:
				tmp="invalid input in line "+str( i+1)		
				return tmp
# ------------------------------------------------------------------------------ rator
		m=re.findall("[\+\*\/\-]",l[0])
		try:
			assert(len(m)==1)
		except AssertionError:
				tmp="invalid rator in line "+str( i+1)		
				return tmp
# ------------------------------------------------------------------------------ rand1
		m=re.match("^\w+$",l[1])
		n=re.match("^(\d+)?[_](\d+)?$",l[1])
		if n:
			tmp="invalid rand1 in line "+str( i+1)		
			return tmp
		else :
			try:
				m.group()
			except AttributeError:
				tmp="invalid rand1 in line "+str( i+1)		
				return tmp
# ------------------------------------------------------------------------------- rand2
		m=re.match("^\w+$",l[2])
		n=re.match("^(\d+)?[_](\d+)?$",l[2])
		if n:
			tmp="invalid rand2 in line "+str( i+1)		
			return tmp
		else :
			try:
				m.group()
			except AttributeError:
				tmp="invalid rand2 in line "+str( i+1)		
				return tmp
# -------------------------------------------------------------------------------- rand3
		m=re.match("[a-z](\w+)?",l[3],re.I)
		try:
			m.group()
		except AttributeError :
				tmp="invalid rand3 in line "+str( i+1)		
				return tmp
		

#a=address_instructions("1.txt")
#print a
