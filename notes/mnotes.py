import sys
import re
import os
import webbrowser
from htmltag import table,tr,td

__all__ = ["clearnotes","getnotes","new_note","delete_note_by_title","getnotes"]

__notes__=[]

c=0

def clearnotes():
	global __notes__,c
	c=0
	__notes__ = [] 

def getnotes():
	return __notes__

def new_note(title,body):
	global __notes_,c
	if title is '' :
		c=c+1
		title = "untitle"+" "+str(c)
	s=(title,body)
	__notes__.append(s)

def delete_note_by_title(title):
	count=len(__notes__)
	i=0
	while i < count :
		i=i+1
		if __notes__[i-1][0] == title:
			del __notes__[i-1]
			if count > len(__notes__):
				count = len(__notes__)
				i=i-1
	
def find_note(key):
	count = len(__notes__)
	found=[]
	if  key == "untitle" :
		for i in range(0,count):
			t=__notes__[i][0]
			p=re.compile("untitle")
			m=p.search(t)
			if m :
				found.append(__notes__[i][1])
		
	else :
		for i in range(0,count):
			t=__notes__[i][0]
			b=__notes__[i][1]
			p=re.compile(key)
			m=p.search(t)
			n=p.search(b)
			if m or n :
				found.append(__notes__[i][1])
	return found

def save_notes():
	global __notes__
	os.remove("notes.html")
	f = open('notes.html','a')
	for i in range(0,len(__notes__)):
		message = table(tr(td("title:"),td(__notes__[i][0]),td("body:"),td(__notes__[i][1]),id="row"+str(i)))
		f.write(message)
	f.close()
	webbrowser.open_new_tab('notes.html')



new_note("kwhr","ajwf")
clearnotes()
new_note("uard","iueofdfsww iyiy lssdlkj")
new_note("uard","manasa")
new_note("iyewiy","qwoieqw kljelq qoiru")
new_note("","ousoua")
new_note("","oeiurow 9798787")
#delete_note_by_title("uard")
save_notes()
""" delete_note_by_title("kwhr")
print __notes__  
a=find_note("untitle 1")
print a
a=find_note("878")
print a """
