import sys

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__limit__ = 10

def copytext(text):
    global __text__
    __text__ = text
   if len(__text__) > __limit__ :
	tmp = tempfile.mktemp(prefix='temp')
	tmp.seek(0)
	tmp.write(__text__)
	tmp.close()

def copyblob(blob):
    global __blob__
    __blob__ = blob
 if len(__text__) > __limit__ :
        tmp = tempfile.mktemp(prefix='temp')
        tmp.seek(0)
        tmp.write(__blob__)
        tmp.close()


def gettext():
    global __text__
    return __text__

def getsize():
    global __size__
    return __size__

def getblob():
    global __blob__
    return __blob__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None


##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
