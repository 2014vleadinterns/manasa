import mnotes

def test_clearnotes():
	mnotes.clearnotes()
	assert(len(mnotes.getnotes()) == 0)

def test_getnotes():
	mnotes.clearnotes()
	title="abc"
	body="i am here"
	mnotes.new_note(title,body)
	memo=mnotes.getnotes()
	assert(len(memo) == 1)
	assert(memo[0][0] == title)
	assert(memo[0][1] == body )
	
def test_new_note():
	mnotes.clearnotes()
	title="abc"
	body="i am here"
	memo=mnotes.getnotes()
	length = len(memo)
	mnotes.new_note(title,body)
	assert(memo[length][0] is  title)
	assert(memo[length][1] is  body)

def test_delete_note_by_title():
	title="abc"
	memo=mnotes.getnotes()
	lenbef=len(memo)
	if lenbef > 0:
		for i in range(0,len(mnotes.getnotes())):
			memo=mnotes.getnotes()
			if memo[i][0] == title:
				mnotes.delete_note_by_title(title)
				lenaf=len(mnotes.getnotes())
				assert(lenaf == lenbef-1 )
			else:
				lenaf=len(mnotes.getnotes())
				assert(lenaf == lenbef)

def test_delete_note_by_title_1():
	mnotes.clearnotes()
	mnotes.new_note("uard","jedj")
	mnotes.new_note("uard","ioquw woiw")
	mnotes.new_note("uard","ljsfwj 0043")
	mnotes.delete_note_by_title("uard")
	assert(len(mnotes.getnotes()) == 0)
		

def test_find_note_1():
	mnotes.clearnotes()
	mnotes.new_note("","jedj")
	mnotes.new_note("","ioquw woiw")
	d = mnotes.find_note("untitle")
	assert(len(d) == 2)
	
def test_find_note_2():
	mnotes.clearnotes()
	mnotes.new_note("","jedj")
	mnotes.new_note("","ioquw woiw")
	d = mnotes.find_note("untitle 1")
	assert(len(d) == 1)
	assert(d[0] == "jedj")
	
def test_find_note_3():
	mnotes.clearnotes()
	mnotes.new_note("uard","jedj")
	mnotes.new_note("uard","ioquw woiw")
	d = mnotes.find_note("uard")
	assert(len(d) == 2)
	 


test_clearnotes()
test_getnotes()
test_new_note()
test_delete_note_by_title()
test_delete_note_by_title_1()
test_find_note_1()
test_find_note_2()
test_find_note_3()
	
	
