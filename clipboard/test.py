# -*- coding: UTF-8 -*-
import clipboard

def run_clipboard_tests():

__limit__ = 20

	def test_empty_clipboard():
        	assert(clipboard.gettext() == None)
        	assert(clipboard.getblob() == None)
    
    	def test_reset_clipboard():
        	clipboard.reset()
        	assert(clipboard.gettext() == None)
        	assert(clipboard.getblob() == None)

    	def test_copy_hindi_text():
        	clipboard.reset()
        	msg = u'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        	clipboard.copytext(msg)
        	text = clipboard.gettext()
        	assert(msg == text)

    	def test_copy_english_text():
        	clipboard.reset()
        	clipboard.copytext("hello, world!")
        	text = clipboard.gettext()
        	assert(text == "hello, world!")

    	def test_copy_text():
		clipboard.reset()
        	text="ansdkasndn"
		clipboard.copytext(text)
        	msg=clipboard.gettext()
        	assert(msg == text)
		assert(len(msg) <= 20)

    	def test_copy_blob():
		clipboard.reset()
		data=[1,2,3,4]
        	clipboard.copyblob(data)
		blob=clipboard.getblob()
		assert(blob==data)

    	def test_recursive_copy_text():
		text1=clipboard.gettext()
		if text1 is not None:
	    		text2="abcd"
	    		clipboard.copytext(text2)
	    		assert(text2 == clipboard.gettext())
			clipboard.reset()
			assert(clipboard.gettext() == None)
			assert(clipboard.getblob() == None )
 
    	def test_recursive_copy_blob():
		blob1=clipboard.getblob()
		if blob1 is not None:
	    		blob2=[1,'a','v','%']
	    		clipboard.copyblob(blob2)
	    		assert(blob2 == clipboard.getblob())
			clipboard.reset()
			assert(clipboard.gettext() == None)
			assert(clipboard.getblob() == None )

    	def test_text_copy_blob():
		text= clipboard.gettext() 
		blob= clipboard.getblob() 
		if blob is None and text is not None :
	    		clipboard.copyblob([1,'w',3])
	    		blob=clipboard.getblob()
	    		assert(blob is not None and blob == [1,'w',3])
	    		assert(text == clipboard.gettext() )
			clipboard.reset()
			assert(clipboard.gettext() == None)
			assert(clipboard.getblob() == None )

    	def test_blob_copy_text():
		blob= clipboard.getblob() 
		text= clipboard.gettext() 
		if text is None and blob is not None :
	    		clipboard.copytext("kjhoy")
	    		text=clipboard.gettext()
	    		assert(text is not None and text == "kjhoy")
	    		assert(blob == clipboard.getblob() )
			clipboard.reset()
			assert(clipboard.gettext() == None)
			assert(clipboard.getblob() == None )

    	def test_blob_recursive_copy_text():
		blob= clipboard.getblob() 
		text= clipboard.gettext() 
		if text is not None and blob is not None :
	    		clipboard.copytext("kjhoy")
	    		text=clipboard.gettext()
	    		assert(text == "kjhoy")
	    		assert(blob is not None)
			clipboard.reset()
			assert(clipboard.gettext() == None)
			assert(clipboard.getblob() == None )

    	def test_text_recursive_copy_blob():
		blob= clipboard.getblob() 
		text= clipboard.gettext() 
		if text is not None and blob is not None :
	    		clipboard.copyblob([1,'w',3])
	    		blob=clipboard.getblob()
	    		assert(text is not None )
	    		assert(blob == [1,'w',3])
			clipboard.reset()
			assert(clipboard.gettext() == None)
			assert(clipboard.getblob() == None )

	test_empty_clipboard()
	test_reset_clipboard()
	test_copy_english_text()
	test_copy_hindi_text()
	test_copy_text()
	test_copy_blob()
	test_recursive_copy_text()
	test_recursive_copy_blob()
	test_text_copy_blob()
	test_blob_copy_text()
	test_blob_recursive_copy_text()
	test_text_recursive_copy_blob()

run_clipboard_tests()

def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
